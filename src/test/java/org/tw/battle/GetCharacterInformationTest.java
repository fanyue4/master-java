package org.tw.battle;

import org.jooq.meta.derby.sys.Sys;
import org.junit.jupiter.api.Test;
import org.tw.battle.dbTest.InMemoryDbSupport;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.Game;

import static org.junit.jupiter.api.Assertions.*;
import static org.tw.battle.GameTestHelper.createCharacter;
import static org.tw.battle.GameTestHelper.createGame;

@InMemoryDbSupport
class GetCharacterInformationTest {
    @Test
    void should_get_character_information_by_its_id() {
        final Game game = createGame();
        Integer id = createCharacter(game);

        final CommandResponse getInfoResponse = game.execute("character-info", id.toString());
        assertTrue(getInfoResponse.isSuccess());
        assertEquals(
            String.format("id: %d, name: unnamed, hp: 100, x: 0, y: 0, status: alive", id),
            getInfoResponse.getMessage());
    }

    @Test
    void should_fail_if_no_id_is_provided() {
        final Game game = createGame();

        final CommandResponse getInfoResponse = game.execute("character-info");
        assertFalse(getInfoResponse.isSuccess());
        assertEquals(
            "Bad command: character-info <character id>",
            getInfoResponse.getMessage());
    }

    @Test
    void should_fail_if_more_arguments_are_provided() {
        final Game game = createGame();
        Integer id = createCharacter(game);

        final CommandResponse getInfoResponse = game.execute("character-info", id.toString(), "additional");
        assertFalse(getInfoResponse.isSuccess());
        assertEquals(
            "Bad command: character-info <character id>",
            getInfoResponse.getMessage());
    }

    @Test
    void should_fail_if_character_id_does_not_exist() {
        final Game game = createGame();
        final String notExistId = "9999";

        final CommandResponse getInfoResponse = game.execute("character-info", notExistId);
        assertFalse(getInfoResponse.isSuccess());
        assertEquals("Bad command: character not exist", getInfoResponse.getMessage());
    }
}
