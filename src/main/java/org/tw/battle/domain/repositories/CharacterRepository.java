package org.tw.battle.domain.repositories;

import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.*;

/**
 * @author Liu Xia
 */
public class CharacterRepository {
    private final ServiceConfiguration configuration;

    public CharacterRepository(ServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    public int create(String name, int hp, int x, int y) throws Exception {
        // TODO:
        //   Please implement the method.
        //   Please refer to DatabaseConnectionProvider to see how to create connection.
        String query = "INSERT INTO character (name, hp, x, y) VALUES(?, ?, ?, ?)";
        try (Connection connection = DatabaseConnectionProvider.createConnection(configuration);
             PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString (1, name);
            preparedStatement.setInt (2, hp);
            preparedStatement.setInt (3, x);
            preparedStatement.setInt (4, y);
            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        throw new RuntimeException("Delete this line and implement the method.");
    }

    public String getInformation(String id) throws Exception {
        String query = "SELECT * FROM character WHERE id = ?";
        try (Connection connection = DatabaseConnectionProvider.createConnection(configuration);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt (1, Integer.parseInt(id));
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int characterId = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int hp = resultSet.getInt("hp");
                int x = resultSet.getInt("x");
                int y = resultSet.getInt("y");
                return String.format("id: %d, name: %s, hp: %d, x: %d, y: %d, status: %s", characterId, name, hp, x, y, hp > 0 ? "alive" : "dead");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public String updateInformation (int id, String name) throws Exception {
        String query = "UPDATE character SET name = ? WHERE id = ?";
        try (Connection connection = DatabaseConnectionProvider.createConnection(configuration);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, id);
            if (preparedStatement.executeUpdate() > 0) {
                return String.format("Character renamed: id: %d, name: %s", id, name);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
