package org.tw.battle.domain;

import java.util.Arrays;
import java.util.List;

/**
 * @author Liu Xia
 *
 * IMPORTANT: You cannot modify this file.
 */
public class Game {
    private final List<CommandHandler> handlers;
    private static final String[] EMPTY_ARGS = new String[0];

    public Game(List<CommandHandler> handlers) {
        this.handlers = handlers;
    }

    public CommandResponse execute(String... commandTokens) {
        String commandName = commandTokens[0];
        String[] arguments = extractArguments(commandTokens);

        return handlers.stream()
                .filter(h -> h.canHandle(commandName))
                .findFirst()
                .map(h -> handleCommand(h, arguments))
                .orElse(CommandResponse.fail("Bad command"));
    }

    private CommandResponse handleCommand(CommandHandler h, String[] arguments) {
        try {
            return h.handle(arguments);
        } catch (Exception error) {
            return CommandResponse.fail("Unhandled error: " + error.getMessage());
        }
    }

    private String[] extractArguments(String[] commandTokens) {
        return commandTokens.length == 1 ?
                EMPTY_ARGS :
                Arrays.copyOfRange(commandTokens, 1, commandTokens.length);
    }
}
