package org.tw.battle.domain;

import org.tw.battle.domain.commands.CreateCharacterCommandHandler;
import org.tw.battle.domain.commands.GetCharacterInformationCommandHandler;
import org.tw.battle.domain.commands.RenameCharacterCommandHandler;
import org.tw.battle.domain.commands.QuitCommandHandler;
import org.tw.battle.domain.repositories.CharacterRepository;

import java.util.Arrays;
import java.util.List;

/**
 * @author Liu Xia
 */
public class CommandHandlerProvider {
    public List<CommandHandler> createHandlers(ServiceConfiguration configuration) {
        CharacterRepository characterRepository = new CharacterRepository(configuration);
        return Arrays.asList(
            new QuitCommandHandler(),
            new CreateCharacterCommandHandler(characterRepository),
                new GetCharacterInformationCommandHandler(characterRepository),
                new RenameCharacterCommandHandler(characterRepository)
        );
    }
}
