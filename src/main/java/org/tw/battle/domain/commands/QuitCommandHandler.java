package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;

/**
 * @author Liu Xia
 */
public class QuitCommandHandler implements CommandHandler {
    private static final String CMD_NAME = "quit";

    @Override
    public boolean canHandle(String commandName) {
        return CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) {
        if (commandArgs.length != 0) {
            return CommandResponse.fail(String.format("Bad command: %s", CMD_NAME));
        }

        return CommandResponse.quit();
    }
}
