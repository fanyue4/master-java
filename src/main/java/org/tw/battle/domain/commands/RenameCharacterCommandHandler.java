package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.repositories.CharacterRepository;

public class RenameCharacterCommandHandler implements CommandHandler {
    private final String CMD_NAME = "rename-character";
    private final CharacterRepository characterRepository;

    public RenameCharacterCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean canHandle(String commandName) {
        return CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {
        if (commandArgs.length != 2 || !commandArgs[1].matches("[a-zA-Z-]+")) {
            return CommandResponse.fail(String.format("Bad command: %s <character id> <new name>", CMD_NAME));
        }

        String message = characterRepository.updateInformation(Integer.parseInt(commandArgs[0]), commandArgs[1]);
        return CommandResponse.success(message);
    }
}
